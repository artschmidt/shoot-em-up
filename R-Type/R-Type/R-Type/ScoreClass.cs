﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type
{
    static class ScoreClass
    {
        public static int score = 0;
        private static int multiplier = 1;
        private static int killingStreet = 0;

        public static int Score
        {
            get
            {
                return score;
            }

            set
            {
                score = value;
            }
        }


        public static void scoreMulti(int points)
        {
            Console.WriteLine("KillingStreet: " + killingStreet);
            killingStreet++;
            if (killingStreet <= 5)
            {
                multiplier = 1;
                score += points * multiplier;
            }
            else
            {
                if (killingStreet > 5 && killingStreet <= 10)
                {
                    multiplier = 2;
                    score += points * multiplier;
                }else
                {
                    if (killingStreet > 10)
                    {
                        multiplier = 3;
                        score += points * multiplier;
                    }
                }
            }
        }

        public static void resetKillingStreet()
        {
            killingStreet = 0;
        }


    }
}
