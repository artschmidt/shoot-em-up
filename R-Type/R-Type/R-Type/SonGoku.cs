﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Text;
using System.Threading.Tasks;
using SFML;
using SFML.System;
using SFML.Graphics;
using SFML.Window;
using R_Type.StatusBars;
using R_Type.Items;

namespace R_Type
{
    class SonGoku
    {
        private RenderWindow window;
        public Vector2f pos;
        private int SSForm = 0;
        private List<Weapon> weaponsToDestroyList = new List<Weapon>();
        private List<Weapon> weaponList = new List<Weapon>();

        private int frame = 0;
        private int sheetColumn = 0;

        private StatusBar healthBar;
        private StatusBar manaBar;
        private StatusBar shotBar;
        private StatusBar speedBar;

        private Texture flying = new Texture(GlobalParameter.ProjectPath + @"\Resources\songokuFlug65x45.png");
        private Texture shootingLeft = new Texture(GlobalParameter.ProjectPath + @"\Resources\sonGokuShotLeft105x65.png");
        private Texture shootingRight = new Texture(GlobalParameter.ProjectPath + @"\Resources\sonGokuShotRight105x65.png");
        private Texture preparingKamehameha = new Texture(GlobalParameter.ProjectPath + @"\Resources\sonGokuKHH44x66.png");
        private Texture performingKamehameha = new Texture(GlobalParameter.ProjectPath + @"\Resources\sonGokuKHHPerforming70x56.png");
        private Texture disappearing = new Texture(GlobalParameter.ProjectPath + @"\Resources\teleportDisappear44x106.png");
        private Texture appearing = new Texture(GlobalParameter.ProjectPath + @"\Resources\teleportAppear44x106.png");
        private Texture takingDamage = new Texture(GlobalParameter.ProjectPath + @"\Resources\sonGokuHit50x60.png");
        private Texture transformingToSS = new Texture(GlobalParameter.ProjectPath + @"\Resources\SSTransformation100x90.png");
        private Texture transformingToSSEnding = new Texture(GlobalParameter.ProjectPath + @"\Resources\SSTransformationEnding100x90.png");

        private Texture shapeTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\redShape.png");
        private Texture gameOverTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\gameOver540x81.png");

        private Boolean isAlive = true;

        private Boolean isFlying = true;

        private Boolean isShooting = false;
        private Boolean isShootingRight = false;
        private Boolean shotAnimationFinished = false;

        private Boolean isPreparingKamehameha = false;
        private Boolean isPerformingKamehameha = false;
        private Boolean kamehamehaPrepared = false;

        private Boolean isDisappearing = false;
        private Boolean isAppearing = false;
        public Boolean allowMoving = true;

        private Boolean isTakingDamage = false;

        public Boolean isSS = false;
        public Boolean isTransformingToSS = false;
        public Boolean isGlowing = false;
        public Boolean transformingToSSIsEnding = false;

        private int transformDuration;
        private int gameOverScreenDuration = 30;

        //private Sprite sonGokuSprite;
        private RectangleShape rectangleShape = new RectangleShape(new Vector2f(38, 45));
        private ConvexShape flyingShape = new ConvexShape();

        private Sprite sonGokuSprite = new Sprite();
        private Vector2f differenceBetweenShootingTextureAndShape = new Vector2f(5, 18);
        private Vector2f differenceBetweenFlyingTextureAndShape = new Vector2f(5, 2);

        public int HP { get; set; }
        public int mana { get; set; }
        private int maxHP = 5;
        private int maxMana = 5;
        public int teleportRange { get; set; }
        public SonGoku()
        {
            pos.X = 100;
            pos.Y = 100;
            HP = 5;
            mana = 5;

            teleportRange = 250;
            sonGokuSprite = new Sprite(flying);
            sonGokuSprite.Origin = new Vector2f();
            sonGokuSprite.Position = pos;

            healthBar = new StatusBar("healthBar", new Vector2f(0, GlobalParameter.windowHeight - 25), 0);
            manaBar = new StatusBar("manaBar", new Vector2f(0, GlobalParameter.windowHeight - 55), 1);
            shotBar = new StatusBar("shotBar", new Vector2f(0, 0), 2);
            speedBar = new StatusBar("speedBar", new Vector2f(0, 30), 3);

            rectangleShape.Texture = shapeTexture;
            rectangleShape.Position = pos + differenceBetweenShootingTextureAndShape;

            flyingShape.Texture = shapeTexture;
            flyingShape.SetPointCount(3);
            flyingShape.SetPoint(0, new Vector2f(44, 0));
            flyingShape.SetPoint(1, new Vector2f(51, 27));
            flyingShape.SetPoint(2, new Vector2f(1, 36));
            flyingShape.Position = pos + differenceBetweenFlyingTextureAndShape;
        }
        public void update()
        {
            if (!isAlive)
            {
                return;
            }

            CheckIfShotIsAlive();
            updateShot();
            manaBar.update();
            healthBar.update();
            shotBar.update();
            speedBar.update();

            if (shotAnimationFinished)
            {
                Vector2f missileSpawnPosition = new Vector2f(pos.X + 80, pos.Y + 23);
                weaponList.Add(new SonGokuBasicShot(missileSpawnPosition, SSForm));
                shotAnimationFinished = false;
                isFlying = true;
            }

            if (kamehamehaPrepared)
            {
                Vector2f missileSpawnPosition = new Vector2f(pos.X + 57, pos.Y - 2);
                weaponList.Add(new Kamehameha(missileSpawnPosition, SSForm));
                kamehamehaPrepared = false;
                isPerformingKamehameha = true;
            }
        }

        public void render(RenderWindow window)
        {
            if (!isAlive) {
                renderGameOverScreen();
                return;
            }
            this.window = window;
            healthBar.render(window);
            manaBar.render(window);
            shotBar.render(window);
            speedBar.render(window);

            if (isShooting)
            {
                renderShooting();
                return;
            }
            if (isPreparingKamehameha)
            {
                renderPreparingKamehameha();
                return;
            }
            if (isPerformingKamehameha)
            {
                renderPerformingKamehameha();
                return;
            }
            if (isDisappearing)
            {
                renderDisappearing();
                return;
            }
            if (isAppearing)
            {
                renderAppearing();
                return;
            }
            if (isTransformingToSS)
            {
                renderTransformingToSS();
                return;
            }
            if (isTakingDamage)
            {
                renderTakingDamage();
                return;
            }
            renderFlying();
        }

        private void renderGameOverScreen()
        {
            frame++;
            if (frame >= 5)
            {
                frame = 0;
                sheetColumn++;
                sheetColumn = sheetColumn % 5;
                gameOverScreenDuration--;
            }
            rectangleShape.TextureRect = new IntRect(0, sheetColumn * 81, 540, 81);

            window.Draw(rectangleShape);

            if (gameOverScreenDuration == 0)
            {
                StartGame.restartGame();
            }
        }

        private void renderFlying()
        {
            frame++;
            if (frame >= 10)
            {
                frame = 0;
                sheetColumn++;
                sheetColumn = sheetColumn % 4;
            }
            sonGokuSprite.TextureRect = new IntRect(65 * sheetColumn, 0, 60, 45);
            window.Draw(sonGokuSprite);

            if (GlobalParameter.showCollisionBoxes)
            {
                window.Draw(flyingShape);
            }
        }

        /********************************************
        ************* Super Saiyajin*****************
        *********************************************/
        public void prepareTransformationToSS()
        {
            if (!isSS && isFlying)
            {
                isTransformingToSS = true;
                allowMoving = false;
                isFlying = false;
                transformDuration = 0;
                SoundEffect.playSound("SSTransformation.wav");

                frame = 0;
                sheetColumn = 0;
                sonGokuSprite.Texture = transformingToSS;
                sonGokuSprite.TextureRect = new IntRect(0, 0, 100, 90);
                sonGokuSprite.Position += new Vector2f(-12, -40);
                rectangleShape.Position += new Vector2f(8, -16);
            }
        }

        private void renderTransformingToSS()
        {
            frame++;
            if (!isGlowing)
            {
                if (frame >= 10 && sheetColumn <= 13)
                {
                    frame = 0;
                    sheetColumn++;
                    sonGokuSprite.TextureRect = new IntRect(100 * sheetColumn, 0, 100, 90);
                }

                if (frame >= 10 && sheetColumn >= 14)
                {
                    frame = 0;
                    sheetColumn++;
                    sonGokuSprite.TextureRect = new IntRect(100 * sheetColumn, 0, 100, 90);
                    if (sheetColumn == 17)
                    {
                        sheetColumn = 13;
                        transformDuration++;
                    }
                    if (transformDuration == 5)
                    {
                        isGlowing = true;
                        sonGokuSprite.Texture = transformingToSSEnding;
                        sonGokuSprite.TextureRect = new IntRect(1300, 0, 100, 90);
                        sheetColumn = 1;
                        transformDuration = 0;
                        sheetColumn = 10;
                        frame = 0;
                    }
                }
            }


            if (isGlowing)
            {

                if (frame >= 10 && sheetColumn >= 10)
                {
                    frame = 0;
                    sonGokuSprite.TextureRect = new IntRect(100 * sheetColumn, 0, 100, 90);
                    sheetColumn++;
                    if (sheetColumn == 14)
                    {
                        sheetColumn = 10;
                        transformDuration++;
                    }
                    if (transformDuration == 5)
                    {
                        transformingToSSIsEnding = true;
                        sheetColumn = 9;
                    }
                }

                if (frame >= 10 && sheetColumn <= 9)
                {
                    frame = 0;
                    sheetColumn--;
                    sonGokuSprite.TextureRect = new IntRect(100 * sheetColumn, 0, 100, 90);
                }

                // transformation to SS finished
                if (transformingToSSIsEnding && isGlowing && transformDuration == 5 && sheetColumn == -1)
                {
                    isTransformingToSS = false;
                    allowMoving = true;
                    isSS = true;
                    isFlying = true;
                    SSForm++;
                    maxHP = 8;
                    HP = 8;
                    mana = 8;
                    SonGokuBasicShot.firingRate = 8;
                    SonGokuBasicShot.projectileSpeed = 34;
                    maxMana = 8;
                    sheetColumn = 0;
                    frame = 0;
                    changeTextureToSS();
                    sonGokuSprite.Texture = flying;
                    sonGokuSprite.Position -= new Vector2f(-12, -40);
                    rectangleShape.Position -= new Vector2f(8, -16);
                }
            }
            window.Draw(sonGokuSprite);
            if (GlobalParameter.showCollisionBoxes)
            {
                window.Draw(rectangleShape);
            }
        }

        private void changeTextureToSS()
        {
            flying = new Texture(GlobalParameter.ProjectPath + @"\Resources\songokuSSFlug65x45.png");
            shootingLeft = new Texture(GlobalParameter.ProjectPath + @"\Resources\sonGokuSSShotLeft105x65.png");
            shootingRight = new Texture(GlobalParameter.ProjectPath + @"\Resources\sonGokuSSShotRight105x65.png");
            preparingKamehameha = new Texture(GlobalParameter.ProjectPath + @"\Resources\sonGokuSSKHH44x66.png");
            performingKamehameha = new Texture(GlobalParameter.ProjectPath + @"\Resources\sonGokuSSKHHPerforming70x56.png");
            disappearing = new Texture(GlobalParameter.ProjectPath + @"\Resources\teleportDisappearSS44x106.png");
            appearing = new Texture(GlobalParameter.ProjectPath + @"\Resources\teleportAppearSS44x106.png");
            takingDamage = new Texture(GlobalParameter.ProjectPath + @"\Resources\sonGokuSSHit50x60.png");
            healthBar = new StatusBar("healthBarSS1", new Vector2f(0, GlobalParameter.windowHeight - 25), 0);
            manaBar = new StatusBar("manaBarSS1", new Vector2f(0, GlobalParameter.windowHeight - 50), 1);
            shotBar = new StatusBar("shotBarSS", new Vector2f(0, 0), 2);
        }

        /********************************************
         ************* Taking damage ****************
        *********************************************/
        public void takeDamage(int damage)
        {
            if (!allowMoving || isTransformingToSS || isPerformingKamehameha)
            {
                return;
            }

            SoundEffect.playSound("sonGokuDamageTaken.wav");

            if (GlobalParameter.SonGokuIsInvincible) {
                return;
            }

            if (HP > 0)
            {
                HP -= damage;
            }

            if (HP <= 0) {
                isAlive = false;
                allowMoving = false;
                rectangleShape = new RectangleShape(new Vector2f(540, 81));
                rectangleShape.Texture = gameOverTexture;
                rectangleShape.Position = new Vector2f(GlobalParameter.windowWidth / 2 - rectangleShape.Texture.Size.X/2, GlobalParameter.windowHeight / 2 - rectangleShape.Texture.Size.Y/10);
            }

            if (SonGokuBasicShot.projectileSpeed > SonGokuBasicShot.projectileSpeedStartValue)
            {
                SonGokuBasicShot.projectileSpeed -= ProjectileSpeedItem.increasedProjectileSpeedAmount;
            }

            if (SonGokuBasicShot.firingRate > 3)
            {
                SonGokuBasicShot.firingRate--;
            }

            if (!isFlying)
            {
                return;
            }
            ScoreClass.resetKillingStreet();
            isTakingDamage = true;
            isFlying = false;

            frame = 0;
            sheetColumn = 1;

            sonGokuSprite.TextureRect = new IntRect(0, 0, 50, 60);
            sonGokuSprite.Texture = takingDamage;
        }

        private void renderTakingDamage()
        {
            frame++;
            if (frame >= 6)
            {
                frame = 0;
                sonGokuSprite.TextureRect = new IntRect(50 * sheetColumn, 0, 50, 60);
                sheetColumn++;
            }

            if (sheetColumn == 4)
            {
                sheetColumn = 0;
                isTakingDamage = false;
                isFlying = true;
                sonGokuSprite.Texture = flying;
            }
            window.Draw(sonGokuSprite);
            if (GlobalParameter.showCollisionBoxes)
            {
                window.Draw(rectangleShape);
            }
        }

        /********************************************
         ************* Gain Mana/Live **************
        *********************************************/
        public void gainMana()
        {
            if (mana == maxMana)
            {
                return;
            }
            mana++;
        }

        public void gainHP()
        {
            if (HP == maxHP)
            {
                return;
            }
            HP++;
        }

        /********************************************
        ****************** SHOT *********************
        *********************************************/
        public void initializeSonGokuBasicShot()
        {
            if (!allowMoving)
            {
                return;
            }
            if (isFlying && SonGokuBasicShot.isNewInstanceAllowed())
            {
                isShooting = true;
                isFlying = false;

                frame = 0;
                sheetColumn = 1;
                sonGokuSprite.TextureRect = new IntRect(0, 0, 105, 65);

                if (isShootingRight)
                {
                    sonGokuSprite.Texture = shootingRight;
                    isShootingRight = false;
                }
                else
                {
                    sonGokuSprite.Texture = shootingLeft;
                    isShootingRight = true;
                }
            }
        }

        private void renderShooting()
        {

            sonGokuSprite.TextureRect = new IntRect(105 * sheetColumn, 0, 105, 65);
            sheetColumn++;

            if (sheetColumn == 11)
            {
                sheetColumn = 0;
                isShooting = false;
                shotAnimationFinished = true;
                sonGokuSprite.Texture = flying;
            }
            window.Draw(sonGokuSprite);
            if (GlobalParameter.showCollisionBoxes)
            {
                window.Draw(rectangleShape);
            }
        }

        private void updateShot()
        {
            foreach (Weapon weapon in weaponList)
            {
                weapon.update();
            }
        }

        public void removeElement(Weapon shot)
        {
            weaponList.Remove(shot);
            if (shot is SonGokuBasicShot)
            {
                SonGokuBasicShot.decrementNumberOfShotsAlive();
            }
            shot = null;
        }

        public List<Weapon> getWeaponList()
        {
            return weaponList;
        }
        private void CheckIfShotIsAlive()
        {
            foreach (Weapon s in weaponList)
            {
                if (!s.isAlive)
                {
                    weaponsToDestroyList.Add(s);
                }
            }
            foreach (Weapon s in weaponsToDestroyList)
            {
                removeElement(s);
            }
            weaponsToDestroyList.Clear();
        }
        /********************************************
         ***************** TELEPORT *****************
         *********************************************/
        public void teleport(Vector2f rigthLeft, Vector2f upDown)
        {
            if (!allowMoving || !isFlying)
            {
                return;
            }
            pos = sonGokuSprite.Position;
            pos.Y -= 46;
            pos.X += 15;
            setPosition();

            pos.X -= teleportRange * rigthLeft.Y;
            pos.X += teleportRange * rigthLeft.X;
            pos.Y += teleportRange * upDown.Y;
            pos.Y -= teleportRange * upDown.X;

            isDisappearing = true;
            allowMoving = false;
            isFlying = false;

            frame = 0;
            sheetColumn = 1;
            SoundEffect.playSound("teleport.wav");
            sonGokuSprite.Texture = disappearing;
            sonGokuSprite.TextureRect = new IntRect(0, 0, 44, 106);
            ifOutOfScreenReset();
            rectangleShape.Size = new Vector2f(1, 45);
        }

        private void renderDisappearing()
        {
            frame++;
            if (frame >= 5)
            {
                frame = 0;
                sonGokuSprite.TextureRect = new IntRect(106 * sheetColumn, 0, 44, 106);
                sheetColumn++;
            }

            if (sheetColumn == 3)
            {
                isDisappearing = false;
                isAppearing = true;
                sheetColumn = 0;
                frame = 0;
                setPosition();

                sonGokuSprite.Texture = appearing;
                sonGokuSprite.TextureRect = new IntRect(0, 0, 44, 106);
            }

            window.Draw(sonGokuSprite);
            if (GlobalParameter.showCollisionBoxes)
            {
                window.Draw(rectangleShape);
            }
        }

        private void renderAppearing()
        {
            frame++;
            if (frame >= 5)
            {
                frame = 0;
                sonGokuSprite.TextureRect = new IntRect(106 * sheetColumn, 0, 44, 106);
                sheetColumn++;
            }
            if (sheetColumn == 2)
            {
                isAppearing = false;
                allowMoving = true;
                isFlying = true;
                sheetColumn = 0;
                frame = 0;
                pos.Y += 46;
                pos.X -= 15;
                setPosition();
                sonGokuSprite.Texture = flying;
                rectangleShape.Size = new Vector2f(38, 45);
            }
            window.Draw(sonGokuSprite);
            if (GlobalParameter.showCollisionBoxes)
            {
                window.Draw(rectangleShape);
            }
        }

        /********************************************
        ***************** KAMEHAMEHA *****************
        *********************************************/
        public void prepareKamehameha()
        {
            if (allowMoving && isFlying && mana >= 5)
            {
                mana -= Kamehameha.manaCost;
                isPreparingKamehameha = true;
                allowMoving = false;
                isFlying = false;
                SoundEffect.playSound("kamehamehaShort.wav");
                frame = 0;
                sheetColumn = 0;
                sonGokuSprite.Texture = preparingKamehameha;
                sonGokuSprite.TextureRect = new IntRect(0, 0, 44, 66);
                sonGokuSprite.Position += new Vector2f(16, -17);
                rectangleShape.Position += new Vector2f(16, -16);
            }
        }

        private void renderPerformingKamehameha()
        {
            frame++;
            if (frame >= 10)
            {
                frame = 0;
                sonGokuSprite.TextureRect = new IntRect(70 * sheetColumn, 0, 70, 56);
                sheetColumn++;
                sheetColumn = sheetColumn % 4;
            }

            window.Draw(sonGokuSprite);
            if (GlobalParameter.showCollisionBoxes)
            {
                window.Draw(rectangleShape);
            }
        }

        private void renderPreparingKamehameha()
        {
            frame++;
            if (frame >= 15)
            {
                frame = 0;
                sheetColumn++;
                sonGokuSprite.TextureRect = new IntRect(44 * sheetColumn, 0, 44, 66);
            }
            if (sheetColumn == 8)
            {
                isPreparingKamehameha = false;
                kamehamehaPrepared = true;
                sheetColumn = 0;
                frame = 0;
                sonGokuSprite.Texture = performingKamehameha;
                sonGokuSprite.TextureRect = new IntRect(0, 0, 70, 56);
                sonGokuSprite.Position += new Vector2f(6, 10);
            }
            window.Draw(sonGokuSprite);
            if (GlobalParameter.showCollisionBoxes)
            {
                window.Draw(rectangleShape);
            }
        }

        public void finishKamehameha()
        {
            sonGokuSprite.Texture = flying;
            isPerformingKamehameha = false;
            allowMoving = true;
            sonGokuSprite.Position -= new Vector2f(22, -7);
            rectangleShape.Position -= new Vector2f(16, -16);
            isFlying = true;
        }

        /********************************************
        ******************* MOVING *****************
        *********************************************/
        public void moveY(Boolean up)
        {
            if (!allowMoving)
            {
                return;
            }
            pos = sonGokuSprite.Position;

            if (up)
            {
                pos.Y -= 5;
            }
            else
            {
                pos.Y += 5;
            }

            ifOutOfScreenReset();
            setPosition();
        }

        public void moveX(Boolean left)
        {
            if (!allowMoving)
            {
                return;
            }
            pos = sonGokuSprite.Position;

            if (left)
            {
                pos.X -= 5;
            }
            else
            {
                pos.X += 5;
            }

            ifOutOfScreenReset();
            setPosition();
        }

        private void setPosition()
        {
            sonGokuSprite.Position = pos;
            rectangleShape.Position = pos + differenceBetweenShootingTextureAndShape;
            flyingShape.Position = pos + differenceBetweenFlyingTextureAndShape;
        }

        private void ifOutOfScreenReset()
        {
            // 30, weil beim Teleportieren wird eine größe Textur benutzt, als beim Fliegen.
            if (pos.X < 0)
            {
                pos.X = 0;
            }
            if (pos.X > GlobalParameter.windowWidth - sonGokuSprite.TextureRect.Width)
            {
                pos.X = GlobalParameter.windowWidth - sonGokuSprite.TextureRect.Width;
            }

            if (pos.Y < -30)
            {
                pos.Y = 0 - 30;
            }
            if (pos.Y > GlobalParameter.windowHeight - sonGokuSprite.TextureRect.Height)
            {
                pos.Y = GlobalParameter.windowHeight - sonGokuSprite.TextureRect.Height;
            }
        }
        public Shape getSonGokuShape()
        {
            if (isFlying)
            {
                return flyingShape;
            }
            return rectangleShape;
        }
    }
}