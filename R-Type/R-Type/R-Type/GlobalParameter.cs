﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type
{
    static class GlobalParameter
    {
        public static String ProjectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
        public static uint windowWidth = 1280;
        public static uint windowHeight = 720;
        public static Boolean showCollisionBoxes = false;
        public static bool allowSounds = true;
        public static bool SonGokuIsInvincible = true;
    }
}
