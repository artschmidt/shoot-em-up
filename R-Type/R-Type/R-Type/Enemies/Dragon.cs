﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type
{
    class Dragon : Enemy
    {
        static Texture flyTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\dragonFlying51x51.png");
        static Texture attackTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\dragonAttacking54x51.png");


        private Sprite creepSprite = new Sprite(flyTexture);
        private static Vector2f velocity = new Vector2f(-3, -3);
        private static int health = 2;
        private static int points = 5;

        private Boolean isflying = true;
        private Boolean isAttacking = false;

        private int frame = 0;
        private int sheetColumn = 0;
        private const int maxAttackDurationInFrames = 50;
        private int attackDurationInFrames = maxAttackDurationInFrames;


        public Dragon(int startPosition)
            : base(health, new Vector2f(-1, RandomNumberGenerator.GetRandomNumber(-3, 3)), points)
        {

            creepSprite.Origin = new Vector2f();
            base.EnemySprite = creepSprite;

            int delayDraw = startPosition;

            creepSprite.TextureRect = new IntRect(51 * sheetColumn, 0, 51, 51);

            Position = new Vector2f(GlobalParameter.windowWidth + (creepSprite.TextureRect.Width * startPosition) + 10, 100);
        }

        public override void update()
        {

            move();
            if (!isAttacking && RandomNumberGenerator.GetRandomNumber(1, 150) == 1)
            {
                ram();
            }
        }

        private void ram()
        {
            isflying = false;
            isAttacking = true;
            MovementSpeed -= new Vector2f(10, 0);

            creepSprite.Texture = attackTexture;
            creepSprite.TextureRect = new IntRect(54 * sheetColumn, 0, 54, 51);
        }

        public override void move()
        {
            if (!hasCollidedWithSonGoku && HP > 0 && CollisionManager.collisionBetweenObjects(creepSprite, StartGame.getSonGoku().getSonGokuShape()))
            {
                hasCollidedWithSonGoku = true;
                StartGame.getSonGoku().takeDamage(1);
                ScoreClass.resetKillingStreet();
            }

            if (Position.Y + creepSprite.TextureRect.Height >= GlobalParameter.windowHeight || Position.Y <= 0)
            {
                Vector2f v;
                v = MovementSpeed;
                v.Y *= -1;
                MovementSpeed = v;
            }


            Position += MovementSpeed;
        }

        public override void takeDamage(int amount)
        {
            HP -= amount;
            if (HP < 1)
            {
                IsAlive = false;
            }
        }

        public override void render(RenderWindow window, float d)
        {
            if (isflying)
            {
                renderFlying(window);
            }
            if (isAttacking)
            {
                renderPreparingAttack(window);
            }
            creepSprite.Position = Position + MovementSpeed * d;
            window.Draw(EnemySprite);
        }

        private void renderFlying(RenderWindow window)
        {
            frame++;
            if (frame >= 5)
            {
                frame = 0;
                sheetColumn++;
                sheetColumn = sheetColumn % 5;
                creepSprite.TextureRect = new IntRect(51 * sheetColumn, 0, 51, 51);
            }
        }

        private void renderPreparingAttack(RenderWindow window)
        {
            frame++;
            attackDurationInFrames--;
            if (frame >= 4)
            {
                frame = 0;
                sheetColumn++;
                if (sheetColumn == 6) {
                    sheetColumn = 4;
                }
                creepSprite.TextureRect = new IntRect(54 * sheetColumn, 0, 54, 51);
            }

            if (attackDurationInFrames == 0)
            {
                isAttacking = false;
                isflying = true;
                MovementSpeed += new Vector2f(10, 0);
                frame = 0;
                sheetColumn = 0;
                attackDurationInFrames = maxAttackDurationInFrames;
                creepSprite.Texture = flyTexture;
                creepSprite.TextureRect = new IntRect(51 * sheetColumn, 0, 51, 51);
            }
        }
    }
}
