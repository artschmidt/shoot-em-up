﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type
{
    class Rock : Enemy
    {
        private static Texture texture = new Texture(GlobalParameter.ProjectPath + @"\Resources\dummyRock.png");

        private Sprite rockSprite = new Sprite(texture);
        private static Vector2f velocity = new Vector2f(-7, 0);
        private static int health = 1;
        private static int points = 1;
        public Rock()
            : base(health, new Vector2f(-7, RandomNumberGenerator.GetRandomNumber(-5,5)), points)
        {
            rockSprite.Origin = new Vector2f();
            base.EnemySprite = rockSprite;

            Position = new Vector2f(GlobalParameter.windowWidth - 1, RandomNumberGenerator.GetRandomNumber(0, (int) GlobalParameter.windowHeight));
        }

        public override void update()
        {
            if (!hasCollidedWithSonGoku && HP > 0 && CollisionManager.collisionBetweenObjects(rockSprite, StartGame.getSonGoku().getSonGokuShape()))
            {
                HP = 0;
                hasCollidedWithSonGoku = true;
                IsAlive = false;
                StartGame.getSonGoku().takeDamage(1);
            }
            move();
        }

        public override void move()
        {
            Position += MovementSpeed;
        }
        
        public override void render(RenderWindow window, float d)
        {
         rockSprite.Position = Position + MovementSpeed * d;
         rockSprite.Rotation += 2;
         window.Draw(EnemySprite);
        }
        public override void takeDamage(int amount)
        {
            HP -= amount;
            if (HP < 1)
            {
                IsAlive = false;
            }
        }
    }
}
