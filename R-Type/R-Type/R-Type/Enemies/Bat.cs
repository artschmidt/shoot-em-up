﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type
{
    class Bat : Enemy
    {
        static Texture flyTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\batFlying70x80.png");
        static Texture attackTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\batAttacking70x70.png");


        private Sprite creepSprite = new Sprite(flyTexture);
        private static Vector2f velocity = new Vector2f(-3, -3);
        private static int health = 1;
        private int firingRate = 1;
        private static int points = 3;


        private Boolean isflying = true;
        private Boolean isPreparingAttack = false;
        private Boolean attackIsPrepared = false;

        private Vector2f parameter;
        private int frame = 0;
        private int sheetColumn = 0;
        private float speed;
        private String movement;
        public Bat(Vector2f startPosition, String movement, float speed, Vector2f parameter)
            : base(health, new Vector2f(-3, RandomNumberGenerator.GetRandomNumber(-3, 3)), points)
        {
            creepSprite.Origin = new Vector2f();
            base.EnemySprite = creepSprite;
            this.speed = speed;
            this.parameter = parameter;
            this.movement = movement;
            creepSprite.TextureRect = new IntRect(70 * sheetColumn, 0, 70, 80);

            if (movement.Equals("straightForward"))
                Position = parameter;
            else Position = startPosition;
        }

        public override void update()
        {
            if (attackIsPrepared)
            {
                attackIsPrepared = false;
                isflying = true;
                int distanceBetweenShipAndShot = 10;
                Vector2f missileSpawnPosition = new Vector2f(creepSprite.Position.X - distanceBetweenShipAndShot,
                    creepSprite.Position.Y + creepSprite.GetGlobalBounds().Height / 2);
                WeaponManager.getWeaponList().Add(new EnemyShot(missileSpawnPosition, Number, new Vector2f(-10, 0)));
                NumberOfShotsAlive++;
                frame = 0;
                sheetColumn = 0;
                creepSprite.Texture = flyTexture;
                creepSprite.TextureRect = new IntRect(70 * sheetColumn, 0, 70, 80);

            }
            move();
            if (!isPreparingAttack && RandomNumberGenerator.GetRandomNumber(1, 100) == 1)
            {
                shot();
            }
        }

        private void shot()
        {
            if (NumberOfShotsAlive < firingRate)
            {
                isflying = false;
                isPreparingAttack = true;
                creepSprite.Texture = attackTexture;
                creepSprite.TextureRect = new IntRect(70 * sheetColumn, 0, 70, 80);
            }
        }

        public override void move()
        {
            if (!hasCollidedWithSonGoku && HP > 0 && CollisionManager.collisionBetweenObjects(creepSprite, StartGame.getSonGoku().getSonGokuShape()))
            {
                hasCollidedWithSonGoku = true;
                StartGame.getSonGoku().takeDamage(1);
            }
            switch (movement)
            {
                case ("ellipseUp"):
                    Position = ellipseMovementUp(Position.X, speed, parameter.X, parameter.Y);
                    break;
                case("sinus"): 
                    Position = sinusMovement(Position.X, speed, parameter);
                    break;
                case ("ellipseDown"):
                    Position = ellipseMovementDown(Position.X, speed, parameter.X, parameter.Y);
                    break;
                case("straightForward"):
                    Position = straightForward(Position, speed);
                    break;
                default:
                    Position = straightForward(Position, speed);
                    break;
            }
        }

        public override void takeDamage(int amount)
        {
            HP -= amount;
            if (HP < 1)
            {
                IsAlive = false;
            }
        }

        public override void render(RenderWindow window, float d)
        {
            if (isflying)
            {
                renderFlying(window);
            }
            if (isPreparingAttack)
            {
                renderPreparingAttack(window);
            }
            creepSprite.Position = Position + MovementSpeed * d;
            window.Draw(EnemySprite);
        }

        private void renderFlying(RenderWindow window)
        {
            frame++;
            if (frame >= 5)
            {
                frame = 0;
                sheetColumn++;
                sheetColumn = sheetColumn % 5;
                creepSprite.TextureRect = new IntRect(70 * sheetColumn, 0, 70, 80);
            }
        }

        private void renderPreparingAttack(RenderWindow window)
        {
            frame++;
            if (frame >= 2)
            {
                frame = 0;
                sheetColumn++;
                creepSprite.TextureRect = new IntRect(70 * sheetColumn, 0, 70, 80);
            }

            if (sheetColumn == 15)
            {
                attackIsPrepared = true;
                isPreparingAttack = false;
            }
        }
    }
}
