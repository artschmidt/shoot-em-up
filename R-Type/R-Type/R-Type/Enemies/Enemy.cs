﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML;
using SFML.System;
using SFML.Audio;
using SFML.Graphics;
using SFML.Window;

namespace R_Type
{
    abstract class Enemy
    {
        public int Points { get; set; }
        private Boolean isAlive = true;
        // sorgt bei der Kollisionsabfrage dafür, dass SonGoku nur einmal Schaden erleidet.
        public Boolean hasCollidedWithSonGoku = false;
        public int HP { get; set; }
        public Vector2f MovementSpeed { get; set; }
        public Vector2f Position { get; set; }
        public static int enemyCounter = 0;
        public int Number { get; set; }
        public int NumberOfShotsAlive { get; set; }
        
        public Sprite EnemySprite { get; set; }
        public bool IsAlive
        {
            get
            {
                return isAlive;
            }
            set
            {
                isAlive = value;
            }
        }

        public Enemy(int hp, Vector2f velocity, int points)
        {
            Points = points;
            HP = hp;
            MovementSpeed = velocity;
            NumberOfShotsAlive = 0;
            Number = enemyCounter;
            enemyCounter++;
        }

        public Vector2f sinusMovement(float currentXPosition, float speed, Vector2f parameter)
        {
            return new Vector2f(currentXPosition - speed, (parameter.X - 85) * (float)(Math.Sin((currentXPosition - speed) / 100)) + parameter.Y);
        }

        public Vector2f ellipseMovementUp(float currentXPosition, float speed, float a, float b)
        {
            float y = 0;
            float x = GlobalParameter.windowWidth - (currentXPosition - speed);
            float yy = b*b - (x/a*x/a)*b*b;
            if (yy >= 0)
            {
                y = (float)Math.Sqrt(yy);
            }
            else {
                y = (float)-Math.Sqrt(-yy);
            }
            return new Vector2f(currentXPosition - speed, y);
        }

        public Vector2f straightForward(Vector2f currentPosition, float speed)
        {
            return new Vector2f(currentPosition.X - speed, currentPosition.Y);
        }

        public Vector2f ellipseMovementDown(float currentXPosition, float speed, float a, float b)
        {
            float y = 0;
            float x = GlobalParameter.windowWidth - (currentXPosition - speed);
            float yy = b * b - (x / a * x / a) * b * b;

            if (yy >= 0)
            {
                y = (float)Math.Sqrt(yy);
            }
            else
            {
                y = (float)-Math.Sqrt(-yy);
            }
            return new Vector2f(currentXPosition - speed, GlobalParameter.windowHeight - y);
        }

        public abstract void move();
        public abstract void update();
        public abstract void takeDamage(int amount);
        public abstract void render(RenderWindow window, float d);
    }
}
