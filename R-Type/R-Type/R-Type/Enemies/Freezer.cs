﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML;
using SFML.System;
using SFML.Audio;
using SFML.Graphics;
using SFML.Window;

namespace R_Type
{
    class Freezer : Enemy
    {
        private static Texture flyling = new Texture(GlobalParameter.ProjectPath + @"\Resources\freezerFlying62x67.png");
        private static Texture teleporting = new Texture(GlobalParameter.ProjectPath + @"\Resources\freezerTeleport63x67.png");
        private static Texture shootingBigShot = new Texture(GlobalParameter.ProjectPath + @"\Resources\freezerBigShot164x112.png");
        private static Texture shootingPlanetBomb = new Texture(GlobalParameter.ProjectPath + @"\Resources\freezerPlanetBomb73x73.png");
        private static Texture shootingSpreadShot = new Texture(GlobalParameter.ProjectPath + @"\Resources\freezerBasicShot60x67.png");

        private static Vector2f velocity = new Vector2f(0, 0);
        private static int health = 100;
        private static int points = 100;
        private int firingRate = 10;
        private int frame = 0;
        private int sheetColumn = 0;
        private int planetBombSheetColumn = 0;

        private Boolean isDisappearing = false;
        private Boolean isAppearing = false;
        private Boolean isPreparingBigShot = false;
        private Boolean fireBigShot = false;

        private Boolean isPreparingPlanetBomb = false;
        private Boolean firePlanetBomb = false;
        private Boolean kuririnDiesSoundPlayed = false;

        private Boolean isPreparingSpreadShot = false;
        private Boolean fireSpreadShot = false;

        Sprite freezerSprite = new Sprite(flyling);

        public Freezer()
            : base(health, velocity, points)
        {
            freezerSprite.Origin = new Vector2f();
            base.EnemySprite = freezerSprite;

            Position = new Vector2f(GlobalParameter.windowWidth - freezerSprite.TextureRect.Width - 100, 250);
        }

        public override void update()
        {
            if (KuririnEvent.kuririnEventActive)
            {
                KuririnEvent.update();
            }

            if (fireBigShot)
            {
                fireBigShot = false;
                Vector2f missileSpawnPosition = new Vector2f(freezerSprite.Position.X - 30,
                freezerSprite.Position.Y + 13);
                WeaponManager.getWeaponList().Add(new BigShot(missileSpawnPosition, Number, new Vector2f(-50, 0)));
                return;
            }

            if (firePlanetBomb)
            {
                firePlanetBomb = false;
                Vector2f missileSpawnPosition = new Vector2f(freezerSprite.Position.X - 180,
                freezerSprite.Position.Y - 70);
                WeaponManager.getWeaponList().Add(new FreezerPlanetBomb(missileSpawnPosition, Number, new Vector2f(-50, 0)));
                return;
            }

            if (fireSpreadShot)
            {
                fireSpreadShot = false;
                spreadShot();
                return;
            }

            if (isAppearing || isDisappearing || isPreparingBigShot || isPreparingPlanetBomb || isPreparingSpreadShot)
            {
                return;
            }
            move();
            if (StartGame.getSonGoku().isTransformingToSS)
            {
                return;
            }

            int rndAction = RandomNumberGenerator.GetRandomNumber(1, 50);

            if (rndAction == 1 || rndAction == 2 || rndAction == 10)
            {
                SoundEffect.playSound("spreadShot.wav");
                isPreparingSpreadShot = true;
                freezerSprite.Texture = shootingSpreadShot;
                freezerSprite.TextureRect = new IntRect(0, 0, 60, 67);
                frame = 0;
                sheetColumn = 0;
                return;
            }
            if (HP < 85 && (rndAction == 3 || rndAction == 4))
            {
                SoundEffect.playSound("bigShot.wav");
                isPreparingBigShot = true;
                freezerSprite.Texture = shootingBigShot;
                freezerSprite.TextureRect = new IntRect(0, 0, 164, 112);
                frame = 0;
                sheetColumn = 0;
                Position += new Vector2f(-100, -24);
                freezerSprite.Position = Position;
                return;
            }
            if (rndAction == 5 || rndAction == 6 /*|| rndAction == 7 || rndAction == 8*/)
            {
                teleport();
                return;
            }
            if (HP < 70 && rndAction == 9)
            {
                if (!kuririnDiesSoundPlayed)
                {
                    kuririnDiesSoundPlayed = true;
                    SoundEffect.playSound("kuririnDies.wav");
                }
                SoundEffect.playSound("planetShot.wav");
                isPreparingPlanetBomb = true;
                freezerSprite.Texture = shootingPlanetBomb;
                freezerSprite.TextureRect = new IntRect(0, 0, 73, 73);
                frame = 0;
                sheetColumn = 0;
                return;
            }
        }

        public override void takeDamage(int amount)
        {
            if (isPreparingPlanetBomb) {
                return;
            }

            HP -= amount;
            if (HP < 1)
            {
                IsAlive = false;
            }
        }

        private void shot()
        {
            if (NumberOfShotsAlive < firingRate)
            {
                int distanceBetweenShipAndShot = 10;
                Vector2f missileSpawnPosition = new Vector2f(freezerSprite.Position.X - distanceBetweenShipAndShot,
                    freezerSprite.Position.Y + freezerSprite.GetGlobalBounds().Height / 2);
                WeaponManager.getWeaponList().Add(new FreezerShot(missileSpawnPosition, Number, new Vector2f(-20, 0)));
                NumberOfShotsAlive++;
            }
        }

        private void spreadShot()
        {
            int distanceBetweenFreezerAndShot = 10;
            Vector2f firstMissileSpawnPosition = new Vector2f(freezerSprite.Position.X - distanceBetweenFreezerAndShot,
                freezerSprite.Position.Y + freezerSprite.GetGlobalBounds().Height / 2);

            for (int i = 0; i < 5; i++)
            {
                WeaponManager.getWeaponList().Add(new FreezerShot(firstMissileSpawnPosition, Number, new Vector2f(-10, (i - 2) * 2)));
                firstMissileSpawnPosition += new Vector2f(0, 10);
            }

            NumberOfShotsAlive++;
        }

        private void teleport()
        {
            SoundEffect.playSound("teleportFreezer.wav");
            float newPosX = RandomNumberGenerator.GetRandomNumber((int)GlobalParameter.windowWidth - 550, (int)GlobalParameter.windowWidth - (int)freezerSprite.TextureRect.Width);
            //float newPosY = RandomNumberGenerator.GetRandomNumber(0, (int)GlobalParameter.windowHeight - (int)freezerSprite.TextureRect.Height);
            float newPosY = StartGame.getSonGoku().pos.Y;
            Position = new Vector2f(newPosX, newPosY);
            isDisappearing = true;
            freezerSprite.Texture = teleporting;
            freezerSprite.TextureRect = new IntRect(0, 0, 63, 67);
        }

        public override void move()
        {
            if (freezerSprite.GetGlobalBounds().Height + freezerSprite.Position.Y >= GlobalParameter.windowHeight || freezerSprite.Position.Y <= 0)
            {
                MovementSpeed *= -1;
            }
            Position += MovementSpeed;
        }

        public override void render(RenderWindow window, float d)
        {
            if (KuririnEvent.kuririnEventActive)
            {
                KuririnEvent.render(window);
            }

            if (isPreparingBigShot)
            {
                renderShootingBigShot(window);
                return;
            }
            if (isDisappearing)
            {
                renderDisappearing(window, d);
                return;
            }
            if (isAppearing)
            {
                renderAppearing(window);
                return;
            }
            if (isPreparingPlanetBomb)
            {
                renderShootingPlanetBomb(window);
                return;
            }
            if (isPreparingSpreadShot)
            {
                renderShootingSpreadShot(window);
                return;
            }
            frame++;
            if (frame >= 5)
            {
                frame = 0;
                freezerSprite.TextureRect = new IntRect(62 * sheetColumn, 0, 62, 67);
                sheetColumn++;
                sheetColumn = sheetColumn % 4;
            }
            freezerSprite.Position = Position + MovementSpeed * d;
            window.Draw(EnemySprite);
        }

        private void renderShootingSpreadShot(RenderWindow window)
        {
            frame++;
            if (frame >= 3)
            {
                frame = 0;
                freezerSprite.TextureRect = new IntRect(60 * sheetColumn, 0, 60, 67);
                freezerSprite.Position = Position;
                sheetColumn++;
            }
            if (sheetColumn == 5 && frame == 2)
            {
                fireSpreadShot = true;
                isPreparingSpreadShot = false;
                sheetColumn = 0;
                frame = 0;
                freezerSprite.Texture = flyling;
                freezerSprite.TextureRect = new IntRect(0, 0, 62, 67);
                freezerSprite.Position = Position;
            }

            window.Draw(freezerSprite);
        }

        private void renderShootingBigShot(RenderWindow window)
        {
            frame++;
            if (frame >= 3)
            {
                frame = 0;
                freezerSprite.TextureRect = new IntRect(164 * sheetColumn, 0, 164, 112);
                freezerSprite.Position = Position;
                sheetColumn++;
            }
            if (sheetColumn == 9 && frame == 2)
            {
                fireBigShot = true;
            }

            if (sheetColumn == 10 && frame == 2)
            {
                isPreparingBigShot = false;
                sheetColumn = 0;
                frame = 0;
                freezerSprite.Texture = flyling;
                freezerSprite.TextureRect = new IntRect(0, 0, 62, 67);
                Position += new Vector2f(100, 24);
                freezerSprite.Position = Position;
            }

            window.Draw(freezerSprite);
            /*if (GlobalParameter.showCollisionBoxes)
            {
                window.Draw(shootingShape);
            }*/
        }

        private void renderShootingPlanetBomb(RenderWindow window)
        {
            frame++;
            if (sheetColumn < 4 && frame >= 20)
            {
                frame = 0;
                sheetColumn++;
                freezerSprite.TextureRect = new IntRect(73 * sheetColumn, 0, 73, 73);
                freezerSprite.Position = Position;
            }
            if (sheetColumn == 4 && frame == 5)
            {
                firePlanetBomb = true;
                sheetColumn++;
            }

            if (!FreezerPlanetBomb.isReady && sheetColumn >= 4 && frame >= 6)
            {
                frame = 0;
                planetBombSheetColumn++;

                if (planetBombSheetColumn == 2)
                {
                    planetBombSheetColumn = 0;
                }
                freezerSprite.TextureRect = new IntRect(73 * (6 + planetBombSheetColumn), 0, 73, 73);
                freezerSprite.Position = Position;
            }

            if (FreezerPlanetBomb.isReady)
            {
                isPreparingPlanetBomb = false;
                sheetColumn = 0;
                frame = 0;
                freezerSprite.Texture = flyling;
                freezerSprite.TextureRect = new IntRect(0, 0, 62, 67);
                //Position += new Vector2f(100, 24);
                freezerSprite.Position = Position;
            }

            window.Draw(freezerSprite);
            /*if (GlobalParameter.showCollisionBoxes)
            {
                window.Draw(shootingShape);
            }*/
        }

        private void renderDisappearing(RenderWindow window, float d)
        {
            frame++;
            if (frame >= 20)
            {
                frame = 0;
                isDisappearing = false;
                isAppearing = true;
                freezerSprite.Position = Position + MovementSpeed * d;
            }
            window.Draw(freezerSprite);
        }

        private void renderAppearing(RenderWindow window)
        {
            frame++;
            if (frame >= 20)
            {
                frame = 0;
                isAppearing = false;
                freezerSprite.Texture = flyling;
                freezerSprite.TextureRect = new IntRect(0, 0, 62, 67);
            }
            window.Draw(freezerSprite);
        }
    }
}
