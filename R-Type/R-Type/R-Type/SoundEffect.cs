﻿using SFML.Audio;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type
{
    static class SoundEffect
    {

        private static int currentTimeInS = 0;
        private static Boolean isPlayingTheme = false;
        private static int lengthOfThemeInS = 90;

        public static void playSound(string strWavName)
        {
            if (GlobalParameter.allowSounds)
            {
                SoundBuffer soundBuffer = new SoundBuffer(GlobalParameter.ProjectPath + "\\Resources\\" + strWavName);
                Sound sound = new Sound(soundBuffer);
                sound.Play();
            }
        }

        public static void replayTheme(Double gameTime)
        {
            currentTimeInS = (int)gameTime % lengthOfThemeInS;
            if (currentTimeInS > 2) {
                return;
            }
            if (currentTimeInS == 0 && !isPlayingTheme)
            {
                SoundEffect.playSound(@"backgroundTheme.wav");
                isPlayingTheme = true;
            }
            if (currentTimeInS == 1) {
                isPlayingTheme = false;
            }
        }
    }
}
