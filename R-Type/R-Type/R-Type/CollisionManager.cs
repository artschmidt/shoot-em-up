﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type
{
    class CollisionManager
    {
        public static Boolean collisionBetweenObjects(Sprite sprite1, Shape sprite2)
        {
            if (sprite1.GetGlobalBounds().Intersects((sprite2.GetGlobalBounds())))
            {
                return true;
            }
            return false;
        }

        public static Boolean collisionBetweenObjects(Sprite sprite1, Sprite sprite2)
        {
            if (sprite1.GetGlobalBounds().Intersects((sprite2.GetGlobalBounds())))
            {
                return true;
            }
            return false;
        }

        public static Boolean isOutOfWindowScreen(Sprite sprite)
        {
            if (sprite.Position.X + sprite.TextureRect.Width < 0)
            {
                return true;
            }

            if (sprite.Position.X > GlobalParameter.windowWidth)
            {
                return true;
            }

            if (sprite.Position.Y + sprite.TextureRect.Height < 0)
            {
                return true;
            }

            if (sprite.Position.Y > GlobalParameter.windowHeight)
            {
                return true;
            }

            return false;
        }

        public static Boolean isOutOfWindowScreen(Shape shape)
        {
            if (shape.Position.X + shape.TextureRect.Width < 0)
            {
                return true;
            }

            if (shape.Position.X > GlobalParameter.windowWidth)
            {
                return true;
            }

            if (shape.Position.Y + shape.TextureRect.Height < 0)
            {
                return true;
            }

            if (shape.Position.Y > GlobalParameter.windowHeight)
            {
                return true;
            }

            return false;
        }

        public static Boolean enemyPassWestNorthSouth(Sprite sprite)
        {
            if (sprite.Position.X + sprite.TextureRect.Width < 0)
            {
                return true;
            }

            if (sprite.Position.Y + sprite.TextureRect.Height < 0)
            {
                return true;
            }

            if (sprite.Position.Y > GlobalParameter.windowHeight)
            {
                return true;
            }
            return false;
        }

        /*public static Boolean testCollision(RectangleShape shot, CircleShape enemy)
        {
            if(shot.Position.X + 20 <= enemy.Position.X + 30
                && shot.Position.Y + 1 <= enemy.Position.Y + 30
                && shot.Position.X >= enemy.Position.X -30
                && shot.Position.Y >= enemy.Position.Y - 30)
            {
                return true;
            }
            return false;
        }*/

    }
}
