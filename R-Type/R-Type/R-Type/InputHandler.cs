﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type
{
    class InputHandler
    {

        private bool moveUp = false;
        private bool moveDown = false;
        private bool moveRight = false;
        private bool moveLeft = false;
        private bool shoot = false;
        private bool transformationToSS = false;
        private bool kamehameha = false;
        private bool controlIsPushed = false;
        private bool spaceReleased = true;
        private bool returnReleased = true;
        private RenderWindow window;
        private SonGoku sonGoku;

        public InputHandler(RenderWindow window)
        {
            this.window = window;
            window.KeyPressed += window_KeyPressed;
            window.KeyReleased += window_KeyReleased;
            window.Closed += window_closed;
        }

        public void HandleInput()
        {
            if (controlIsPushed)
            {
                handleInputForTeleportation();
                return;
            }
            if (moveUp && !moveDown)
                sonGoku.moveY(true);
            if (moveDown && !moveUp)
                sonGoku.moveY(false);
            if (moveRight && !moveLeft)
                sonGoku.moveX(false);
            if (moveLeft && !moveRight)
                sonGoku.moveX(true);
            if (shoot && spaceReleased)
            {
                sonGoku.initializeSonGokuBasicShot();
                shoot = false;
                spaceReleased = false;
            }
            if (kamehameha && returnReleased)
            {
                sonGoku.prepareKamehameha();
                kamehameha = false;
                returnReleased = false;
            }
            if (transformationToSS)
            {
                transformationToSS = false;
                sonGoku.prepareTransformationToSS();
            }
        }

        private void handleInputForTeleportation()
        {
            int trueCounter = 0;
            int moveUpMarker = 0;
            int moveDownMarker = 0;
            int moveRightMarker = 0;
            int moveLeftMarker = 0;

            if (moveUp)
            {
                trueCounter++;
                moveUpMarker = 1;
            }
            if (moveDown)
            {
                trueCounter++;
                moveDownMarker = 1;
            }
            if (moveRight)
            {
                trueCounter++;
                moveRightMarker = 1;
            }
            if (moveLeft)
            {
                trueCounter++;
                moveLeftMarker = 1;
            }

            if (trueCounter > 2 || trueCounter == 0)
            {
                return;
            }
            sonGoku.teleport(new Vector2f(moveRightMarker, moveLeftMarker), new Vector2f(moveUpMarker, moveDownMarker));
        }

        private void window_KeyReleased(object sender, KeyEventArgs e)
        {
            sonGoku = StartGame.getSonGoku();
            switch (e.Code)
            {
                case (Keyboard.Key.Up):
                    moveUp = false;
                    break;
                case (Keyboard.Key.Down):
                    moveDown = false;
                    break;
                case (Keyboard.Key.Right):
                    moveRight = false;
                    break;
                case (Keyboard.Key.Left):
                    moveLeft = false;
                    break;
                case (Keyboard.Key.Space):
                    spaceReleased = true;
                    shoot = false;
                    break;
                case (Keyboard.Key.Return):
                    returnReleased = true;
                    kamehameha = false;
                    break;
                case (Keyboard.Key.LControl):
                    controlIsPushed = false;
                    break;
                case (Keyboard.Key.X):
                    window.Close();
                    break;
                case (Keyboard.Key.Escape):
                    StartGame.tooglePause();
                    break;
                case (Keyboard.Key.F2):
                    StartGame.restartGame();
                    break;
            }
        }

        private void window_closed(object sender, EventArgs e)
        {
            window.Close();
        }

        private void window_KeyPressed(object sender, KeyEventArgs e)
        {
            sonGoku = StartGame.getSonGoku();
            switch (e.Code)
            {
                case (Keyboard.Key.Up):
                    moveUp = true;
                    break;
                case (Keyboard.Key.Down):
                    moveDown = true;
                    break;
                case (Keyboard.Key.Right):
                    moveRight = true;
                    break;
                case (Keyboard.Key.Left):
                    moveLeft = true;
                    break;
                case (Keyboard.Key.Space):
                    shoot = true;
                    break;
                case (Keyboard.Key.Return):
                    kamehameha = true;
                    break;
                case (Keyboard.Key.S):
                    if (!sonGoku.isSS)
                    {
                        transformationToSS = true;
                    }
                    break;
                case (Keyboard.Key.LControl):
                    controlIsPushed = true;
                    break;
            }
        }
    }
}
