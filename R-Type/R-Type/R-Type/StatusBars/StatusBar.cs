﻿using R_Type.Items;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type.StatusBars
{
    class StatusBar
    {
        static Texture healthBarTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\healthBar.png");
        static Texture healthBarSS1Texture = new Texture(GlobalParameter.ProjectPath + @"\Resources\healthBarSS1.png");
        static Texture healthBarSS2Texture = new Texture(GlobalParameter.ProjectPath + @"\Resources\healthBarSS2.png");
        static Texture healthBarSS3Texture = new Texture(GlobalParameter.ProjectPath + @"\Resources\healthBarSS3.png");
        static Texture manaBarTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\manaBar.png");
        static Texture manaBarSS1Texture = new Texture(GlobalParameter.ProjectPath + @"\Resources\manaBarSS1.png");
        static Texture manaBarSS2Texture = new Texture(GlobalParameter.ProjectPath + @"\Resources\manaBarSS2.png");
        static Texture manaBarSS3Texture = new Texture(GlobalParameter.ProjectPath + @"\Resources\manaBarSS3.png");
        static Texture shotBarTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\shotBar.png");
        static Texture shotBarSSTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\shotBarSS_145.png");
        static Texture speedBarTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\speedBar.png");

        
        private int type;
        Sprite barSprite = new Sprite();
        //type 0 = healthBar, type 1 = manaBar, type 2 = shotBar, type 3 = speedBar
        public StatusBar(String form, Vector2f pos, int type)
        {
            barSprite.Position = pos;
            this.type = type;

            switch (form)
            {
                case ("healthBar"):
                    barSprite.Texture = healthBarTexture;
                    break;
                case ("healthBarSS1"):
                    barSprite.Texture = healthBarSS1Texture;
                    break;
                case ("healthBarSS2"):
                    barSprite.Texture = healthBarSS2Texture;
                    break;
                case ("healthBarSS3"):
                    barSprite.Texture = healthBarSS3Texture;
                    break;
                case ("manaBar"):
                    barSprite.Texture = manaBarTexture;
                    break;
                case ("manaBarSS1"):
                    barSprite.Texture = manaBarSS1Texture;
                    break;
                case ("manaBarSS2"):
                    barSprite.Texture = manaBarSS2Texture;
                    break;
                case ("manaBarSS3"):
                    barSprite.Texture = manaBarSS3Texture;
                    break;
                case ("shotBar"):
                    barSprite.Texture = shotBarTexture;
                    break;
                case ("shotBarSS"):
                    barSprite.Texture = shotBarSSTexture;
                    break;
                case ("speedBar"):
                    barSprite.Texture = speedBarTexture;
                    break;
                default:
                    break;
            }
        }

        public void update()
        {

            if (type == 0) {
                updateHealthBar();
                return;
            }
            if (type == 1)
            {
                updateManaBar();
                return;
            }
            if (type == 2)
            {
                updateShotBar();
                return;
            }
            if (type == 3)
            {
                updateSpeedBar();
                return;
            }
        }

        private void updateHealthBar()
        {
            int HP = StartGame.getSonGoku().HP;
            barSprite.TextureRect = new IntRect(0, 25 * HP, (int)barSprite.Texture.Size.X, 25);
        }
        private void updateManaBar()
        {
            int mana = StartGame.getSonGoku().mana;
            barSprite.TextureRect = new IntRect(0, 25 * mana, (int) barSprite.Texture.Size.X, 25);
        }
        private void updateShotBar()
        {
            int shots = SonGokuBasicShot.firingRate-SonGokuBasicShot.numberOfShotsAlive;
            barSprite.TextureRect = new IntRect(0, 25 * shots, (int)barSprite.Texture.Size.X, 25);
        }
        private void updateSpeedBar()
        {
            if (SonGokuBasicShot.projectileSpeed == SonGokuBasicShot.projectileSpeedStartValue)
            {
                barSprite.TextureRect = new IntRect(0, 0, (int)barSprite.Texture.Size.X, 25);
                return;
            }

            int projectileSpeed = (SonGokuBasicShot.projectileSpeed - SonGokuBasicShot.projectileSpeedStartValue) / ProjectileSpeedItem.increasedProjectileSpeedAmount;
            barSprite.TextureRect = new IntRect(0, 25 * projectileSpeed, (int)barSprite.Texture.Size.X, 25);
        }
        public void render(RenderWindow window)
        {
            window.Draw(barSprite);
        }
    }
}
