﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type
{
    // wird abgespielt, wenn Freezer das erste mal die PlanetBomb schießt
    static class KuririnEvent
    {
        static Texture movingTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\kuririnMovement50x36.png");
        static Texture blockingTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\kuririnBlocking36x39.png");
        static Texture dyingTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\kuririnDead61x67.png");

        public static Boolean kuririnEventActive = false;
        public static Vector2f pos;
        private static float endPosX;
        private static Vector2f velocity;
        public static Sprite kuririnSprite = new Sprite(movingTexture);
        public static Boolean endPositionReached = false;
        public static Boolean eventDone = false;
        private static int frame = 0;
        private static int sheetColumn = 0;
        private static int sheetColumnDeadAnimation = 0;

        public static void startKuririnEvent()
        {
            kuririnEventActive = true;
            kuririnSprite.TextureRect = new IntRect(0, 0, 50, 36);
            pos = new Vector2f(-20, GlobalParameter.windowHeight / 3);
            endPosX = 300;
            velocity = new Vector2f(5, 0);
        }

        public static void update()
        {
            if (eventDone)
            {
                return;
            }
            if (pos.X < endPosX)
            {
                pos += velocity;
            }
            else
            {
                endPositionReached = true;
            }
        }

        public static void render(RenderWindow window)
        {
            if (eventDone)
            {
                return;
            }
            frame++;
            if (frame >= 5 && !endPositionReached && FreezerPlanetBomb.firstPlanetBombShot)
            {
                frame = 0;
                kuririnSprite.TextureRect = new IntRect(50 * sheetColumn, 0, 50, 36);
                sheetColumn++;
                sheetColumn = sheetColumn % 4;
            }

            if (frame >= 5 && endPositionReached && FreezerPlanetBomb.firstPlanetBombShot)
            {
                frame = 0;
                kuririnSprite.Texture = blockingTexture;
                kuririnSprite.TextureRect = new IntRect(36 * sheetColumn, 0, 36, 39);
                sheetColumn++;
                sheetColumn = sheetColumn % 4;
            }
            if (frame >= 5 && !FreezerPlanetBomb.firstPlanetBombShot)
            {
                frame = 0;
                kuririnSprite.Texture = dyingTexture;
                kuririnSprite.TextureRect = new IntRect(61 * sheetColumnDeadAnimation, 0, 61, 67);
                sheetColumnDeadAnimation++;
            }

            if (sheetColumnDeadAnimation == 7 && frame == 4 && !FreezerPlanetBomb.firstPlanetBombShot)
            {
                eventDone = true;
                StartGame.getSonGoku().prepareTransformationToSS();
            }
            kuririnSprite.Position = pos;
            window.Draw(kuririnSprite);
        }
    }
}
