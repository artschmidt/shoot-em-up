﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML;
using SFML.System;
using SFML.Audio;
using SFML.Graphics;
using SFML.Window;
using System.Threading;

namespace R_Type
{
    class Kamehameha : Weapon
    {
        Vector2f posEndOfKamehameha;
        Vector2f posBeginOfKamehameha;
        Vector2f posSonGoku;
        static Texture kamehamehaTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\Kamehameha1280x36.png");
        static Texture kamehamehaTextureSS = new Texture(GlobalParameter.ProjectPath + @"\Resources\KamehamehaSS1280x36.png");

        private static Texture shapeTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\longRedShape.png");
        static Texture texture = new Texture(GlobalParameter.ProjectPath + @"\Resources\sonGokuBasicShot.png");

        private ConvexShape kamehamehaShape = new ConvexShape();

        Sprite kamehamehaSprite = new Sprite();
        
        static int projectileSpeed = 5;
        public static int manaCost = 4;
        private int maxDamage;

        private Boolean reduceKamehameha = false;

        public Kamehameha(Vector2f positionSonGoku, int SSForm)
            : base(1)
        {
            Velocity = new Vector2f(projectileSpeed, 0);
            if (SSForm == 0)
            {
                kamehamehaSprite.Texture = kamehamehaTexture;
                maxDamage = 20;
            }
            if (SSForm == 1)
            {
                kamehamehaSprite.Texture = kamehamehaTextureSS;
                maxDamage = 30;
            }
            kamehamehaSprite.Position = positionSonGoku;
            kamehamehaShape.Position = positionSonGoku;
            posEndOfKamehameha = positionSonGoku;
            posBeginOfKamehameha = positionSonGoku - new Vector2f(positionSonGoku.X, 0);
            posSonGoku = positionSonGoku;
            base.isAlive = true;
            //SoundEffect.playSound(@"shotKiblast.wav");

            kamehamehaShape.Texture = shapeTexture;
            kamehamehaShape.SetPointCount(8);
            kamehamehaShape.SetPoint(0, new Vector2f(posBeginOfKamehameha.X, 16));
            kamehamehaShape.SetPoint(1, new Vector2f(posEndOfKamehameha.X - 35, 16));
            kamehamehaShape.SetPoint(2, new Vector2f(posEndOfKamehameha.X - 35, 14));
            kamehamehaShape.SetPoint(3, new Vector2f(posEndOfKamehameha.X, 14));
            kamehamehaShape.SetPoint(4, new Vector2f(posEndOfKamehameha.X, 22));
            kamehamehaShape.SetPoint(5, new Vector2f(posEndOfKamehameha.X - 35, 22));
            kamehamehaShape.SetPoint(6, new Vector2f(posEndOfKamehameha.X - 35, 20));
            kamehamehaShape.SetPoint(7, new Vector2f(posBeginOfKamehameha.X, 20));

        }

        public override void update()
        {
            move();
        }

        public override void move()
        {
            if (!reduceKamehameha && posEndOfKamehameha.X > GlobalParameter.windowWidth)
            {
                reduceKamehameha = true;
            }

            foreach (Enemy e in LevelManager.getEnemiesList())
            {
                if (maxDamage > 0 && CollisionManager.collisionBetweenObjects(e.EnemySprite, kamehamehaShape))
                {
                    maxDamage--;
                    e.takeDamage(DMG);
                }
            }

            if (reduceKamehameha)
            {
                if (posBeginOfKamehameha.X > GlobalParameter.windowWidth)
                {
                    StartGame.getSonGoku().finishKamehameha();
                    isAlive = false;
                    kamehamehaShape.Position = new Vector2f(GlobalParameter.windowWidth + 1, GlobalParameter.windowHeight + 1);
                }
                posBeginOfKamehameha += Velocity * 10;
                return;
            }

            posEndOfKamehameha += Velocity;
        }

        public override void render(RenderWindow window, float d)
        {
            renderKamehameha();
            window.Draw(kamehamehaSprite);

            if (GlobalParameter.showCollisionBoxes)
            {
                window.Draw(kamehamehaShape);
            }
        }

        private void renderKamehameha()
        {
            int lengthOfKamehameha = (int)(posEndOfKamehameha.X - posSonGoku.X);
            kamehamehaSprite.Position = new Vector2f(posSonGoku.X + posBeginOfKamehameha.X, posSonGoku.Y);
            if (lengthOfKamehameha < 150)
            {
                kamehamehaSprite.TextureRect = new IntRect((int)kamehamehaTexture.Size.X - lengthOfKamehameha, 0, lengthOfKamehameha, 36);
                kamehamehaShape.SetPoint(0, new Vector2f(posBeginOfKamehameha.X, 16));
                kamehamehaShape.SetPoint(1, new Vector2f(lengthOfKamehameha - 35, 16));
                kamehamehaShape.SetPoint(2, new Vector2f(lengthOfKamehameha - 35, 14));
                kamehamehaShape.SetPoint(3, new Vector2f(lengthOfKamehameha, 14));
                kamehamehaShape.SetPoint(4, new Vector2f(lengthOfKamehameha, 22));
                kamehamehaShape.SetPoint(5, new Vector2f(lengthOfKamehameha - 35, 22));
                kamehamehaShape.SetPoint(6, new Vector2f(lengthOfKamehameha - 35, 20));
                kamehamehaShape.SetPoint(7, new Vector2f(posBeginOfKamehameha.X, 20));
                return;
            }
            if (lengthOfKamehameha < 300)
            {
                kamehamehaSprite.TextureRect = new IntRect((int)kamehamehaTexture.Size.X - lengthOfKamehameha, 36, lengthOfKamehameha, 36);
                kamehamehaShape.SetPoint(0, new Vector2f(posBeginOfKamehameha.X, 13));
                kamehamehaShape.SetPoint(1, new Vector2f(lengthOfKamehameha - 35, 13));
                kamehamehaShape.SetPoint(2, new Vector2f(lengthOfKamehameha - 35, 10));
                kamehamehaShape.SetPoint(3, new Vector2f(lengthOfKamehameha, 10));
                kamehamehaShape.SetPoint(4, new Vector2f(lengthOfKamehameha, 26));
                kamehamehaShape.SetPoint(5, new Vector2f(lengthOfKamehameha - 35, 26));
                kamehamehaShape.SetPoint(6, new Vector2f(lengthOfKamehameha - 35, 23));
                kamehamehaShape.SetPoint(7, new Vector2f(posBeginOfKamehameha.X, 23));

                return;
            }
            if (lengthOfKamehameha < 450)
            {
                kamehamehaSprite.TextureRect = new IntRect((int)kamehamehaTexture.Size.X - lengthOfKamehameha, 72, lengthOfKamehameha, 36);
                kamehamehaShape.SetPoint(0, new Vector2f(posBeginOfKamehameha.X, 8));
                kamehamehaShape.SetPoint(1, new Vector2f(lengthOfKamehameha - 35, 8));
                kamehamehaShape.SetPoint(2, new Vector2f(lengthOfKamehameha - 35, 2));
                kamehamehaShape.SetPoint(3, new Vector2f(lengthOfKamehameha, 2));
                kamehamehaShape.SetPoint(4, new Vector2f(lengthOfKamehameha, 34));
                kamehamehaShape.SetPoint(5, new Vector2f(lengthOfKamehameha - 35, 34));
                kamehamehaShape.SetPoint(6, new Vector2f(lengthOfKamehameha - 35, 28));
                kamehamehaShape.SetPoint(7, new Vector2f(posBeginOfKamehameha.X, 28));
                return;
            }
            kamehamehaSprite.TextureRect = new IntRect((int)kamehamehaTexture.Size.X - lengthOfKamehameha, 108, lengthOfKamehameha, 36);
            kamehamehaShape.SetPoint(0, new Vector2f(posBeginOfKamehameha.X, 6));
            kamehamehaShape.SetPoint(1, new Vector2f(lengthOfKamehameha - 35, 6));
            kamehamehaShape.SetPoint(2, new Vector2f(lengthOfKamehameha - 35, 0));
            kamehamehaShape.SetPoint(3, new Vector2f(lengthOfKamehameha, 0));
            kamehamehaShape.SetPoint(4, new Vector2f(lengthOfKamehameha, 36));
            kamehamehaShape.SetPoint(5, new Vector2f(lengthOfKamehameha - 35, 36));
            kamehamehaShape.SetPoint(6, new Vector2f(lengthOfKamehameha - 35, 30));
            kamehamehaShape.SetPoint(7, new Vector2f(posBeginOfKamehameha.X, 30));
        }
    }
}
