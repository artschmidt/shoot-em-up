﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type
{
    abstract class Weapon
    {
        // set private machen und eine methode für dmg hinzugfüfngen
        public int DMG { get; set; }
        public int NumberOfEnemy { get; set; }
        public Vector2f Velocity { get; set; }
        public bool isAlive { get; set; }
        public Weapon(int dmg)
        {
            DMG = dmg;
       }
        public abstract void move();
        public abstract void update();
        public abstract void render(RenderWindow window, float d);

    }
}
