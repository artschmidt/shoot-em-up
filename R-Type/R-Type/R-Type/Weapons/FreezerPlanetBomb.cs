﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML;
using SFML.System;
using SFML.Audio;
using SFML.Graphics;
using SFML.Window;
using System.Threading;

namespace R_Type
{
    class FreezerPlanetBomb : Weapon
    {
        Vector2f pos;
        Vector2f posSonGoku;
        static Texture texture = new Texture(GlobalParameter.ProjectPath + @"\Resources\planetBomb200x200.png");

        Sprite shotSprite = new Sprite(texture);
        static int damage = 4;
        int numberOfShotsAlive = 0;
        static public Boolean isReady = false;
        public static Boolean firstPlanetBombShot = true;
        public Boolean selectFirstTexture = false;

        public int stage = 0;
        private int frame = 0;
        private int framesUntilNextStage = 0;

        public FreezerPlanetBomb(Vector2f posEnemy, int numberOfEnemy, Vector2f velocity)
            : base(damage)
        {
            Velocity = velocity;
            shotSprite.Position = posEnemy;
            NumberOfEnemy = numberOfEnemy;
            pos = posEnemy;
            base.isAlive = true;
            numberOfShotsAlive++;

            if (firstPlanetBombShot)
            {
                StartGame.getSonGoku().allowMoving = false;
                KuririnEvent.startKuririnEvent();
            }

            shotSprite.TextureRect = new IntRect(200 * framesUntilNextStage, 0, 200, 200);
            //SoundEffect.playSound(@"shotKiblast.wav");
        }

        public override void update()
        {
            move();
        }

        public override void move()
        {
            if (!isReady)
            {
                return;
            }
            if (CollisionManager.isOutOfWindowScreen(shotSprite))
            {
                isAlive = false;
                isReady = false;
            }

            if (firstPlanetBombShot && CollisionManager.collisionBetweenObjects(shotSprite, KuririnEvent.kuririnSprite)) {
                SoundEffect.playSound("explosion2.wav");
                isAlive = false;
                isReady = false;
                firstPlanetBombShot = false;
            }

            if (!firstPlanetBombShot && CollisionManager.collisionBetweenObjects(shotSprite, StartGame.getSonGoku().getSonGokuShape()))
            {
                isAlive = false;
                isReady = false;
                StartGame.getSonGoku().takeDamage(DMG);
                ScoreClass.resetKillingStreet();
            }
            pos += Velocity;
        }

        public override void render(RenderWindow window, float d)
        {
            if (!isReady)
            {
                frame++;
                framesUntilNextStage++;

                //frames bis die kugel größer wird
                if (framesUntilNextStage >= 70)
                {
                    stage += 2;
                    framesUntilNextStage = 0;
                }

                if (frame >= 3)
                {
                    frame = 0;
                    // wechselt zwischen den jeweils gleich großen kugeln
                    if (selectFirstTexture)
                    {
                        shotSprite.TextureRect = new IntRect(200 * stage, 0, 200, 200);
                        selectFirstTexture = false;
                    }
                    else
                    {
                        shotSprite.TextureRect = new IntRect(200 * (stage + 1), 0, 200, 200);
                        selectFirstTexture = true;
                    }
                }

                if (stage == 10)
                {
                    SoundEffect.playSound("planetShotFiring.wav");
                    isReady = true;
                    posSonGoku = StartGame.getSonGoku().pos;

                    if (!firstPlanetBombShot)
                    {
                        Velocity = (shotSprite.Position - new Vector2f(0, -shotSprite.Texture.Size.Y / 2) - StartGame.getSonGoku().pos) / 50 * (-1);
                        Velocity = Velocity / (float)(Math.Sqrt(Velocity.X * Velocity.X + Velocity.Y * Velocity.Y));
                        Velocity *= 30;
                    }
                    else
                    {
                        Velocity = (shotSprite.Position - new Vector2f(0, -shotSprite.Texture.Size.Y / 2) - KuririnEvent.pos) / 30 * (-1);
                    }

                }
                window.Draw(shotSprite);
                return;
            }

            shotSprite.Position = shotSprite.Position - (shotSprite.Position - pos + Velocity) + Velocity * d;
            window.Draw(shotSprite);
            //shotSprite.Position = shotSprite.Position + pos - velocity + (velocity) * d;
        }

    }
}
