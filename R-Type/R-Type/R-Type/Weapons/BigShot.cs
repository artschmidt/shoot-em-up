﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML;
using SFML.System;
using SFML.Audio;
using SFML.Graphics;
using SFML.Window;
using System.Threading;

namespace R_Type
{
    class BigShot : Weapon
    {
        Vector2f pos;
        static Texture texture = new Texture(GlobalParameter.ProjectPath + @"\Resources\bigShot.png");

        Sprite shotSprite = new Sprite(texture);
        static int damage = 2;
        int numberOfShotsAlive = 0;

        public BigShot(Vector2f posEnemy, int numberOfEnemy, Vector2f velocity)
            : base(damage)
        {
            Velocity = velocity;
            shotSprite.Position = posEnemy;
            NumberOfEnemy = numberOfEnemy;
            pos = posEnemy;
            base.isAlive = true;
            numberOfShotsAlive++;
            //SoundEffect.playSound(@"shotKiblast.wav");
            Velocity = (shotSprite.Position - new Vector2f(0, -shotSprite.Texture.Size.Y / 2) - StartGame.getSonGoku().pos) / 50 * (-1);
            Velocity = Velocity / (float)(Math.Sqrt(Velocity.X * Velocity.X + Velocity.Y * Velocity.Y));
            Velocity *= 15;
        }

        public override void update()
        {
            move();
        }

        public override void move()
        {
            if (CollisionManager.isOutOfWindowScreen(shotSprite))
            {
                isAlive = false;
            }

            if (CollisionManager.collisionBetweenObjects(shotSprite, StartGame.getSonGoku().getSonGokuShape()))
            {
                isAlive = false;
                StartGame.getSonGoku().takeDamage(DMG);
                ScoreClass.resetKillingStreet();
            }
            pos += Velocity;
        }

        public override void render(RenderWindow window, float d)
        {
            shotSprite.Position = shotSprite.Position - (shotSprite.Position - pos + Velocity) + Velocity * d;
            window.Draw(shotSprite);
            //shotSprite.Position = shotSprite.Position + pos - velocity + (velocity) * d;
        }

    }
}
