﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML;
using SFML.System;
using SFML.Audio;
using SFML.Graphics;
using SFML.Window;
using System.Threading;

namespace R_Type
{
    class FreezerShot : Weapon
    {
        Vector2f pos;
        static Texture texture = new Texture(GlobalParameter.ProjectPath + @"\Resources\dummyFreezerShot.png");

        Sprite shotSprite = new Sprite(texture);
        static int damage = 1;
        int numberOfShotsAlive = 0;

        public FreezerShot(Vector2f posEnemy, int numberOfEnemy, Vector2f velocity)
            : base(damage)
        {
            Velocity = velocity;
            shotSprite.Position = posEnemy;
            NumberOfEnemy = numberOfEnemy;
            pos = posEnemy;
            shotSprite.Rotation = 180;
            base.isAlive = true;
            numberOfShotsAlive++;
            //SoundEffect.playSound(@"shotKiblast.wav");
        }

        public override void update()
        {
            move();
        }

        public override void move()
        {
            if (CollisionManager.isOutOfWindowScreen(shotSprite))
            {
                isAlive = false;
            }

            if (CollisionManager.collisionBetweenObjects(shotSprite, StartGame.getSonGoku().getSonGokuShape()))
            {
                isAlive = false;
                StartGame.getSonGoku().takeDamage(DMG);
            }
            pos += Velocity;
        }

        public override void render(RenderWindow window, float d)
        {
            shotSprite.Position = shotSprite.Position - (shotSprite.Position - pos + Velocity) + Velocity * d;
            window.Draw(shotSprite);
            //shotSprite.Position = shotSprite.Position + pos - velocity + (velocity) * d;
        }

    }
}
