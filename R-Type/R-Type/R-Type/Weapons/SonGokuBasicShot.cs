﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML;
using SFML.System;
using SFML.Audio;
using SFML.Graphics;
using SFML.Window;
using System.Threading;
using R_Type.Items;

namespace R_Type
{
    class SonGokuBasicShot : Weapon
    {
        Vector2f pos;
        static Texture sonGokuBasicShotTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\sonGokuBasicShot.png");
        static Texture sonGokuBasicShotTextureSS = new Texture(GlobalParameter.ProjectPath + @"\Resources\sonGokuSSBasicShot.png");
        static Texture explosionTexture = new Texture(GlobalParameter.ProjectPath + @"\Resources\sonGokuBasicShotExplosion23x28.png");

        Sprite shotSprite = new Sprite(sonGokuBasicShotTexture);
        public static int firingRate = 3;
        public const int projectileSpeedStartValue = 10;
        private static int maxProjectileSpeed = projectileSpeedStartValue + ProjectileSpeedItem.increasedProjectileSpeedAmount * 8;
        public static int projectileSpeed = projectileSpeedStartValue;
        public static int numberOfShotsAlive = 0;
        private static int maxNumberOfShotsAlive = 8;

        private int frame = 0;
        private int sheetColumn = 0;
        private Boolean performExplosion = false;

        public SonGokuBasicShot(Vector2f posSonGoku, int SSForm)
            : base(1)
        {
            Velocity = new Vector2f(projectileSpeed, 0);
            shotSprite.Position = posSonGoku;

            if (SSForm == 0)
            {
                shotSprite.Texture = sonGokuBasicShotTexture;
            }
            if (SSForm == 1)
            {
                shotSprite.Texture = sonGokuBasicShotTextureSS;
                DMG = 2;
            }

            pos = posSonGoku;
            base.isAlive = true;
            numberOfShotsAlive++;
            //SoundEffect.playSound(@"shotKiblast.wav");
        }

        public override void update()
        {
            move();
        }

        public override void move()
        {
            if (performExplosion)
            {
                return;
            }
            if (CollisionManager.isOutOfWindowScreen(shotSprite))
            {
                isAlive = false;
            }

            foreach (Enemy e in LevelManager.getEnemiesList())
            {
                if (CollisionManager.collisionBetweenObjects(shotSprite, e.EnemySprite))
                {
                    performExplosion = true;
                    shotSprite.Texture = explosionTexture;
                    shotSprite.TextureRect = new IntRect(23 * sheetColumn, 0, 23, 28);
                    shotSprite.Position += new Vector2f(10,2);
                    
                    e.takeDamage(DMG);
                }
            }
            pos += Velocity;
        }

        public override void render(RenderWindow window, float d)
        {

            if (!performExplosion)
            {
                shotSprite.Position = shotSprite.Position + (pos - shotSprite.Position - Velocity) + Velocity * d;
            }
            else
            {
                frame++;
                if (frame >= 2)
                {
                    frame = 0;
                    sheetColumn++;
                    shotSprite.TextureRect = new IntRect(23 * sheetColumn, 0, 23, 28);
                }
            }

            if (sheetColumn == 5) {
                isAlive = false;
            }

            // shotSprite.Position + (Abstand zwischen shotSprite.Position und letzten update) + Velocity * Zeit(Wert zwischen 0 und 1)
            window.Draw(shotSprite);
        }

        public static Boolean isNewInstanceAllowed()
        {
            if (firingRate > numberOfShotsAlive)
            {
                return true;
            }
            return false;
        }

        public static void decrementNumberOfShotsAlive()
        {
            numberOfShotsAlive--;
        }

        public static void increaseFiringRate(int amount)
        {
            if (firingRate >= maxNumberOfShotsAlive)
            {
                return;
            }
            firingRate += amount;
        }
        public static void increaseProjectileSpeed(int amount)
        {
            if (projectileSpeed >= maxProjectileSpeed)
            {
                return;
            }
            projectileSpeed += amount;
        }
    }
}
