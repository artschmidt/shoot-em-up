﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type
{
    static class LevelManager
    {
        public static int currentLevel = 1;
        private static List<Enemy> deadEnemies = new List<Enemy>();
        private static List<Enemy> enemies = new List<Enemy>();
        private static List<Spawner> spawnerList= new List<Spawner>();
        private static List<Spawner> removeablespawnerList = new List<Spawner>();
        private static double currentGametime;
        public static void update(double gametime)
        {
            currentGametime = gametime;
            updateSpawnerAndCheckForFinishedSpawner();
            spawnLevel();
            checkForDeadEnemiesAndUpdate();
            removeEnemies();
        }

        private static void spawnLevel()
        {

            if (currentLevel != -1 && RandomNumberGenerator.GetRandomNumber(1, 120) == 1)
            {
                createRock(RandomNumberGenerator.GetRandomNumber(1, 5));
            }

            if (!(spawnerList.Count() == 0) || currentLevel == -1)
            {
                return;
            }

            switch (currentLevel)
            {
                case (1):
                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 10, "sinus", 3, 0.3, (int) GlobalParameter.windowHeight/2, (int)GlobalParameter.windowHeight/2, 0));
                    spawnerList.Add(new Spawner(Spawner.Type.DRAGON, 20, "ellipse", 1, 0.01, 300, 400, 12));

                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 10, "ellipseDown", 3, 0.3, (int)GlobalParameter.windowWidth, (int) GlobalParameter.windowHeight - 200, 28));
                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 10, "ellipseUp", 3, 0.3, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight - 200, 28));

                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 3, "straightForward", 4, 0.6, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight / 3- 100, 38));
                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 3, "straightForward", 4, 0.6, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight*2 / 3 - 100, 38));
                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 3, "straightForward", 4, 0.6, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight - 120, 38));

                    spawnerList.Add(new Spawner(Spawner.Type.DRAGON, 15, "straightForward", 4, 0.1, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight - 120, 45));

                    currentLevel++;
                    break;
                case (2):
                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 30, "straightForward", 9, 0.2, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight/3 - 120,   10));
                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 30, "straightForward", 9, 0.2, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight*2/3 - 120, 10));
                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 30, "straightForward", 9, 0.2, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight - 120, 10));

                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 30, "straightForward", 9, 0.2, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight/3 - 200, 12));
                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 30, "straightForward", 9, 0.2, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight*2/3 - 200, 12));
                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 30, "straightForward", 9, 0.2, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight - 200, 12));

                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 10, "straightForward", 4, 0.4, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight/3 - 120, 17));
                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 10, "straightForward", 4, 0.4, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight*2/3 - 120, 17));
                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 10, "straightForward", 4, 0.4, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight - 120, 17));

                    spawnerList.Add(new Spawner(Spawner.Type.ROCK, 30, "straightForward", 2, 0.1, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight - 120, 20));

                    spawnerList.Add(new Spawner(Spawner.Type.DRAGON, 40, "straightForward", 2, 0.1, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight - 120, 24));

                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 5, "sinus", 3, 1.5, (int)GlobalParameter.windowHeight/3, 200, 34));
                    spawnerList.Add(new Spawner(Spawner.Type.BAT, 5, "sinus", 3, 1, (int)GlobalParameter.windowHeight / 3, (int)GlobalParameter.windowHeight - 300, 34));

                    currentLevel++;
                    break;
                case (3):
                    spawnerList.Add(new Spawner(Spawner.Type.ROCK, 5, "straightForward", 2, 0.1, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight - 120, 10));
                    spawnerList.Add(new Spawner(Spawner.Type.ROCK, 10, "straightForward", 2, 0.1, (int)GlobalParameter.windowWidth, (int)GlobalParameter.windowHeight - 120, 10));
                    spawnerList.Add(new Spawner(Spawner.Type.FREEZER, 1, "sinus", 1, 1.5, (int)GlobalParameter.windowHeight / 3, 200, 15));
                    currentLevel = -1;
                    break;                
                default:
                    break;
            }
        }

        private static void updateSpawnerAndCheckForFinishedSpawner()
        {
            foreach (Spawner s in spawnerList)
            {
                s.update();
                if (s.getSpawnerDone())
                    removeablespawnerList.Add(s);
            }

            foreach (Spawner s in removeablespawnerList)
            {
                spawnerList.Remove(s);
            }
            removeablespawnerList.Clear();
        }

        public static void addEnemy(Enemy e)
        {
            enemies.Add(e);
        }

        public static double getGametime()
        {
            return currentGametime;
        }

        private static void createRock(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                enemies.Add(new Rock());
            }
        }

        private static void checkForDeadEnemiesAndUpdate()
        {
            foreach (Enemy e in enemies)
            {
                if (CollisionManager.enemyPassWestNorthSouth(e.EnemySprite))
                {
                    deadEnemies.Add(e);
                    continue;
                }

                if (!e.IsAlive)
                {
                    ScoreClass.scoreMulti(e.Points);
                    deadEnemies.Add(e);
                    if (e is Rock) {
                        continue;
                    }
                    SoundEffect.playSound(@"enemyDeadGroundhit2.wav");
                }
                else
                {
                    e.update();
                }
            }
        }

        /*Entfernt bereits abgeschossene Gegner, welche in der Liste deadEnemies liegen und leert diese im anschluss."*/
        private static void removeEnemies()
        {
            foreach (Enemy e in deadEnemies)
            {
                if (e is Freezer)
                {
                    SoundEffect.playSound(@"freezerTod.wav");
                }


                //SoundEffect.playSound(@"enemyDeadGroundhit2.wav");
                ItemManager.createRandomItem(e.EnemySprite.Position);

                enemies.Remove(e);
            }
            deadEnemies.Clear();
        }

        public static List<Enemy> getEnemiesList()
        {
            return enemies;
        }

        public static List<Enemy> getDeadEnemiesList()
        {
            return deadEnemies;
        }
        public static List<Spawner> getSpawnerList()
        {
            return spawnerList;
        }

        public static List<Spawner> getRemoveablespawnerList()
        {
            return removeablespawnerList;
        }
    }
}
