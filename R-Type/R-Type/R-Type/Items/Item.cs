﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type.Items
{
    abstract class Item
    {
        public bool isAlive { get; set; }
        public Vector2f MovementSpeed { get; set; }
        public Vector2f Position { get; set; }
        public Sprite ItemSprite { get; set; }

        public Item(Vector2f vector2f)
        {
            MovementSpeed = vector2f;
            isAlive = true;
        }

        public abstract void move();
        public abstract void update();
        public void render(RenderWindow window, float d)
        {
            ItemSprite.Position = Position + MovementSpeed * d;
            window.Draw(ItemSprite);
        }
    }
}
