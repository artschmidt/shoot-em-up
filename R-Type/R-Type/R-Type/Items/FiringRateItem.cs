﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type.Items
{
    class FiringRateItem : Item
    {
        static Texture texture = new Texture(GlobalParameter.ProjectPath + @"\Resources\firingRateItem.png");

        Sprite firingRateItemSprite = new Sprite(texture);

        public FiringRateItem(Vector2f spawnPosition)
            : base(new Vector2f(-5, 0))
        {
            Position = spawnPosition;
            base.ItemSprite = firingRateItemSprite;
        }

        public override void update()
        {
            move();
        }

        public override void move()
        {
            if (CollisionManager.isOutOfWindowScreen(firingRateItemSprite))
            {
                isAlive = false;
                return;
            }

            if (isAlive && CollisionManager.collisionBetweenObjects(firingRateItemSprite, StartGame.getSonGoku().getSonGokuShape()))
            {
                SonGokuBasicShot.increaseFiringRate(1);
                isAlive = false;
                return;
            }
            Position += MovementSpeed;
        }
    }
}
