﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type.Items
{
    class ProjectileSpeedItem : Item
    {
        static Texture texture = new Texture(GlobalParameter.ProjectPath + @"\Resources\ProjectileSpeedItem.png");

        Sprite projectileSpeedItemSprite = new Sprite(texture);
        public static int increasedProjectileSpeedAmount = 3;
        public ProjectileSpeedItem(Vector2f spawnPosition)
            : base(new Vector2f(-5, 0))
        {
            Position = spawnPosition;
            base.ItemSprite = projectileSpeedItemSprite;
        }

        public override void update()
        {
            move();
        }

        public override void move()
        {
            if (CollisionManager.isOutOfWindowScreen(projectileSpeedItemSprite))
            {
                isAlive = false;
                return;
            }

            if (isAlive && CollisionManager.collisionBetweenObjects(projectileSpeedItemSprite, StartGame.getSonGoku().getSonGokuShape()))
            {
                SonGokuBasicShot.increaseProjectileSpeed(increasedProjectileSpeedAmount);
                isAlive = false;
                return;
            }
            Position += MovementSpeed;
        }
    }
}
