﻿using R_Type.Items;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type
{
    static class ItemManager
    {
        private static List<Item> expiredItems = new List<Item>();
        private static List<Item> items = new List<Item>();
        public static int zahl = 0;
		
        public static void update()
        {
            checkForExpiredItemsAndUpdate();
            removeEnemies();
        }

        public static void createRandomItem(Vector2f spawnPosition)
        {
            int itemChooser = RandomNumberGenerator.GetRandomNumber(1,4);
            switch (itemChooser)
            {
                case (1):
                    items.Add(new HealthItem(spawnPosition));
                    break;
                case (2):
                    items.Add(new FiringRateItem(spawnPosition));
                    break;
                case (3):
                    items.Add(new ProjectileSpeedItem(spawnPosition));
                    break;
                case (4):
                    items.Add(new ManaItem(spawnPosition));
                    break;
                default:
                    break;
            }

        }


        private static void checkForExpiredItemsAndUpdate()
        {
            foreach (Item i in items)
            {
                if (CollisionManager.isOutOfWindowScreen(i.ItemSprite) || !i.isAlive)
                {
                    expiredItems.Add(i);
                    continue;
                }
                i.update();
            }
        }

        private static void removeEnemies()
        {
            foreach (Item i in expiredItems)
            {
                /*if (i is HealthItem)
                {
                    SoundEffect.playSound(@"freezerTod.wav");
                }*/

                items.Remove(i);

            }
            expiredItems.Clear();
        }

        public static List<Item> getItemsList()
        {
            return items;
        }
    }
}
