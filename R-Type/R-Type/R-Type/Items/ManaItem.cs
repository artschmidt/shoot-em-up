﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type.Items
{
    class ManaItem : Item
    {
        static Texture texture = new Texture(GlobalParameter.ProjectPath + @"\Resources\manaItem.png");

        Sprite healthItemSprite = new Sprite(texture);

        public ManaItem(Vector2f spawnPosition)
            : base(new Vector2f(-5, 0))
        {
            Position = spawnPosition;
            base.ItemSprite = healthItemSprite;
        }

        public override void update()
        {
            move();
        }

        public override void move()
        {
            if (CollisionManager.isOutOfWindowScreen(healthItemSprite))
            {
                isAlive = false;
                return;
            }

            if (isAlive && CollisionManager.collisionBetweenObjects(healthItemSprite, StartGame.getSonGoku().getSonGokuShape()))
            {
                StartGame.getSonGoku().gainMana();
                isAlive = false;
                return;
            }
            Position += MovementSpeed;
        }
    }
}
