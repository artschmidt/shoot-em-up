﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using SFML;
using SFML.Audio;
using SFML.Graphics;
using SFML.Window;
using System.Collections;
using System.IO;
using System.Media;
using SFML.System;
using R_Type.Items;
using R_Type.StatusBars;

namespace R_Type
{
    class StartGame
    {
        private static SonGoku sonGoku = new SonGoku();
        private Background backgroundsky = new Background(1);
        private Background backgroundPause = new Background(2);
        private static RenderWindow window;
        private readonly double MS_PER_UPDATE = 16;
        private Text score;
        public static Boolean paused = true;
        private static InputHandler inputHandler;

        /* Game-Loop!!!
         */
        public void start()
        {
            window = new RenderWindow(new VideoMode(GlobalParameter.windowWidth, GlobalParameter.windowHeight), "R-Type");
            window.SetActive();
            window.SetVerticalSyncEnabled(true);
            window.SetFramerateLimit(60);

            inputHandler = new InputHandler(window);
            initializeScore();

            Stopwatch stopwatch = new Stopwatch();
            Stopwatch gameTime = new Stopwatch();

            stopwatch.Start();
            gameTime.Start();

            double previous = stopwatch.Elapsed.TotalMilliseconds;
            double lag = 0.0;
            while (window.IsOpen)
            {
                window.DispatchEvents();
                window.Clear();
                inputHandler.HandleInput();

                double current = stopwatch.Elapsed.TotalMilliseconds;
                double elapsed = current - previous;
                previous = current;
                lag += elapsed;

                while (lag >= MS_PER_UPDATE)
                {
                    lag -= MS_PER_UPDATE;
                    update(gameTime.Elapsed.TotalSeconds);
                }
                render((float)(lag / MS_PER_UPDATE));
            }
        }


        /*Ruft nach und nach die jeweiligen Update-Methoden der Klassen Shot, Spaceship, enemy auf
        * Dient nur der Berechnung von Collisionen und der neuen Position der Objekte und NICHT um die Objekte Darzustellen
        */
        private void update(double gametime)
        {
            SoundEffect.replayTheme(gametime);

            if (paused)
            {
                return;
            }

            sonGoku.update();
            LevelManager.update(gametime);
            WeaponManager.updateShot();
            ItemManager.update();
            score.DisplayedString = "Score: " + ScoreClass.Score;
        }

        /* Rendermethode welche sich die Shapes der Gegner, Schüsse, Player holt und sie in im SFML-Renderwindow darstellt.*/
        private void render(float d)
        {
            if (paused)
            {
                backgroundPause.renderIdle(window);
                window.Display();
                return;
            }

            backgroundsky.renderMoving(window);
            sonGoku.render(window);

            foreach (Weapon weapon in sonGoku.getWeaponList())
            {
                weapon.render(window, d);
            }

            foreach (Enemy e in LevelManager.getEnemiesList())
            {
                e.render(window, d);
            }

            foreach (Weapon e in WeaponManager.getWeaponList())
            {
                e.render(window, d);
            }

            foreach (Item i in ItemManager.getItemsList())
            {
                i.render(window, d);
            }
            window.Draw(score);
            window.Display();
        }

        public static SonGoku getSonGoku()
        {
            return sonGoku;
        }


        public void initializeScore()
        {
            score = new Text("Score: " + ScoreClass.Score, new Font(GlobalParameter.ProjectPath + "\\Resources\\arial.ttf"));
            score.Color = new Color(Color.Black);
            score.Position = new Vector2f(GlobalParameter.windowWidth - 200, 5);
        }

        public static void restartGame()
        {
            // an dieser Stelle merkt man, wie ugly der Code teilweise ist :(
            sonGoku = new SonGoku();
            ItemManager.getItemsList().Clear();
            WeaponManager.getWeaponList().Clear();
            LevelManager.getEnemiesList().Clear();
            LevelManager.getDeadEnemiesList().Clear();
            LevelManager.getRemoveablespawnerList().Clear();
            LevelManager.getSpawnerList().Clear();
            LevelManager.currentLevel = 1;
            ScoreClass.score = 0;
            FreezerPlanetBomb.firstPlanetBombShot = true;
            SonGokuBasicShot.firingRate = 3;
            SonGokuBasicShot.projectileSpeed = 10;
            SonGokuBasicShot.numberOfShotsAlive = 0;
            KuririnEvent.kuririnEventActive = false;
            KuririnEvent.endPositionReached = false;
            KuririnEvent.eventDone = false;
        }

        public static void tooglePause()
        {
            if (paused)
            {
                paused = false;
            }
            else
            {
                paused = true;
            }
        }
    }
}
