﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;

namespace R_Type
{
    class Spawner
    {
        private double time; 
        private double spawnFrequency; 
        private Vector2f startVector; 
        private String movement; 
        private int amountOfType; 
        private int delay;
        private int enemycount = 0;

        private Boolean delayOver = false;
        private double currentTime;
        private int speed;
        private Boolean spawnerDone = false;
        public enum Type { BAT, FREEZER, ROCK, DRAGON }

        private Type type;
        public Spawner(Type type, int amountOfType, String movement, int speed,
            double spawnFrequency, int parameter1, int parameter2, int delay)
        {
            this.type = type;
            this.spawnFrequency = spawnFrequency;
            time = LevelManager.getGametime();
            this.amountOfType = amountOfType;
            this.movement = movement;
            this.speed = speed;
            this.startVector = new Vector2f(parameter1, parameter2);
            this.delay = delay;
            update();
        }


        public void update()
        {
            if (enemycount < amountOfType && checkIfTimeToSpawn())
            {
                switch (type)
                {
                    case (Spawner.Type.BAT):
                        LevelManager.addEnemy(new Bat(new Vector2f(GlobalParameter.windowWidth, 400), movement, speed, startVector));
                        break;
                    case (Spawner.Type.DRAGON):
                        LevelManager.addEnemy(new Dragon(20));
                        break;
                    case (Spawner.Type.FREEZER):
                        LevelManager.addEnemy(new Freezer());
                        break;
                    case (Spawner.Type.ROCK):
                        for (; enemycount < amountOfType; enemycount++ )
                            LevelManager.addEnemy(new Rock()); 
                        break;
                }
                enemycount++;
                if (enemycount >= amountOfType)
                    spawnerDone = true;
            }
        }

        public Boolean checkIfTimeToSpawn()
        {
            currentTime = LevelManager.getGametime();
            if (!delayOver && currentTime - time <= delay)
                return false;

            delayOver = true;
            if (currentTime - time >= spawnFrequency)
            {
                time = currentTime;
                return true;
            }
            return false;
        }

        public Boolean getSpawnerDone()
        {
            return spawnerDone;
        }
    }
}
