﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R_Type
{
    static class WeaponManager
    {
        private static List<Weapon> weaponsToDestroyList = new List<Weapon>();
        private static List<Weapon> weaponList = new List<Weapon>();

        public static void updateShot()
        {
            foreach (Weapon weapon in weaponList)
            {
                weapon.update();
                if (!weapon.isAlive)
                {
                    weaponsToDestroyList.Add(weapon);
                }
            }
            foreach (Weapon s in weaponsToDestroyList)
            {
                removeElement(s);
            }
            weaponsToDestroyList.Clear();
        }

        public static void removeElement(Weapon shot)
        {
            foreach (Enemy e in LevelManager.getEnemiesList())
            {
                if (e.Number == shot.NumberOfEnemy) {
                    e.NumberOfShotsAlive--;
                }
            }
            weaponList.Remove(shot);
            shot = null;
        }

        public static List<Weapon> getWeaponList()
        {
            return weaponList;
        }

    }
}
