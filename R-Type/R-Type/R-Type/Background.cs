﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Window;
using SFML.Graphics;

namespace R_Type
{
    class Background : Sprite
    {
        private Texture backgroundSky = new Texture(GlobalParameter.ProjectPath + @"\Resources\Background.png");
        private Texture backgroundPause = new Texture(GlobalParameter.ProjectPath + @"\Resources\LoadingScreen.png");
        private int frame;

        public Background(int index)
        {
            if (index == 1)
                this.Texture = backgroundSky;
            else
                this.Texture = backgroundPause;
        }

        public void renderMoving(RenderWindow window)
        {
            this.TextureRect = new IntRect(frame, 0, 1280, 720);
            frame += 4;
            if (frame == 1280)
                frame = 0;
            window.Draw(this);
        }

        public void renderIdle(RenderWindow window)
        {
            this.TextureRect = new IntRect(frame, 0, 1280, 720);
            window.Draw(this);
        }
    }
}

